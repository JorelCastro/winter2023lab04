import java.util.Scanner;

public class ApplianceStore{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		WashingMachine[] machines = new WashingMachine[4];
		for(int i = 0; i < machines.length; i++){
			System.out.println("Enter the brand:");
			String brand = reader.next();
			//machines[i].setBrand(brand);
			System.out.println("How much energy does it consume?");
			int energyUsage = reader.nextInt();
			//machines[i].setEnergyUsage(energyUsage);
			System.out.println("Enter the price:");
			double price = reader.nextDouble();
			//machines[i].setPrice(price);
			System.out.println("Is it smart? If yes, enter [true]. If not, enter [false].");
			boolean isSmart = reader.nextBoolean();
			//machines[i].setIsSmart(isSmart);
			System.out.println("Does it load from the front or the top?");
			String loadType = reader.next();
			//machines[i].setLoadType(loadType);
			machines[i] = new WashingMachine(brand, energyUsage, price, isSmart, loadType);
		}
		System.out.println(machines[3].getBrand());
		System.out.println(machines[3].getEnergyUsage());
		System.out.println(machines[3].getPrice());
		System.out.println(machines[3].getIsSmart());
		System.out.println(machines[3].getLoadType());
		
		System.out.println("Enter a new brand:");
		String brand = reader.next();
		System.out.println("How much energy does it consume?");
		int energyUsage = reader.nextInt();
		System.out.println("Enter the price:");
		double price = reader.nextDouble();
		System.out.println("Is it smart? If yes, enter [true]. If not, enter [false].");
		boolean isSmart = reader.nextBoolean();
		System.out.println("Does it load from the front or the top?");
		String loadType = reader.next();
		
		machines[3] = new WashingMachine(brand, energyUsage, price, isSmart, loadType);
		
		System.out.println(machines[3].getBrand());
		System.out.println(machines[3].getEnergyUsage());
		System.out.println(machines[3].getPrice());
		System.out.println(machines[3].getIsSmart());
		System.out.println(machines[3].getLoadType());
		machines[0].displayInformation();
		machines[0].promote();
		
		machines[1].applyDiscount(25);
	}
}