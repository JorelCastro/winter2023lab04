public class WashingMachine{
	private String brand;
	private int energyUsage;
	private double price;
	private boolean isSmart;
	private String loadType;
	
	public void displayInformation(){
		System.out.println(brand + " " + loadType + " Load Washing Machine");
		System.out.println("$" + price + " + tax");
		if(isSmart){
			System.out.println("This washing machine includes smart assistant features.");
		}
		if(energyUsage < 500){
			System.out.println("ENERGY STAR® Certified");
		}
	}
	
	public void promote(){
		if(price < 750){
			System.out.println("This " + brand + " washing machine is at an affordable price.");
		}else if(price > 1400){
			System.out.println("This model might be too pricey. You can check other models.");
		}
		if(energyUsage < 500){
			System.out.println("This washing machine is energy efficient, which protects the planet and saves you money.");
		}
		if(isSmart){
			System.out.println("You can use your smartphone to control this machine.");
		}
	}
	
	//Lab 4 code starts here
	public void applyDiscount(int discountPercentage){
		if(checkApplyDiscount(discountPercentage)){
			this.price -= this.price*(discountPercentage/100);
			System.out.println("You have a discount of " + discountPercentage + "%. The price is now " + this.price);
		}
	}
	
	private boolean checkApplyDiscount(int discountPercentage){
		if(discountPercentage > 0){
			return true;
		} else {
			return false;
		}
	}
	
	//Getter methods
	public String getBrand(){
		return this.brand;
	}
	public int getEnergyUsage(){
		return this.energyUsage;
	}
	public double getPrice(){
		return this.price;
	}
	public boolean getIsSmart(){
		return this.isSmart;
	}
	public String getLoadType(){
		return this.loadType;
	}
	
	//Setter methods
	/* public void setBrand(String newBrand){
		this.brand = newBrand;
	} */
	public void setEnergyUsage(int newEnergyUsage){
		this.energyUsage = newEnergyUsage;
	}
	public void setPrice(double newPrice){
		this.price = newPrice;
	}
	public void setIsSmart(boolean newIsSmart){
		this.isSmart = newIsSmart;
	}
	public void setLoadType(String newLoadType){
		this.loadType = newLoadType;
	}
	
	//Constructor
	public WashingMachine(String brand, int energyUsage, double price, boolean isSmart, String loadType){
		this.brand = brand;
		this.energyUsage = energyUsage;
		this.price = price;
		this.isSmart = isSmart;
		this.loadType = loadType;
	}
}